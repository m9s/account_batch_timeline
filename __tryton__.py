#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Timeline',
    'name_de_DE': 'Buchhaltung Stapelbuchung Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Batch Accounting
    - Provides the functionality of module account_timeline
      for module account_batch.
    - Provides a wizard to carry balances forward
    ''',
    'description_de_DE': '''Gültigkeitsdauer für Stapelbuchung
    - Stellt die Merkmale der Gültigkeitsdauer-Module für die Stapelbuchung
      zur Verfügung.
    - Stellt einen Assistenten für den Saldenvortrag zur Verfügung.
''',
    'depends': [
        'account_batch',
        'account_timeline',
    ],
    'xml': [
        'batch.xml',
    ],
    'translation': [
    ],
}
