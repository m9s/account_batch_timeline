#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch"
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Eval, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.batch.line'

    def __init__(self):
        super(Line, self).__init__()

        domain_fiscalyear = ('fiscalyear', '=', Eval('fiscalyear'))

        self.account = copy.copy(self.account)
        if self.account.domain is None:
            self.account.domain = [domain_fiscalyear]
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.account.domain):
                self.account.domain += [domain_fiscalyear]

        if 'fiscalyear' not in self.account.depends:
            self.account.depends = copy.copy(self.account.depends)
            self.account.depends.append('fiscalyear')

        if self.account.on_change is None:
            self.account.on_change = ['date']
        elif 'date' not in self.account.on_change:
            self.account.on_change += ['date']

        self.contra_account = copy.copy(self.contra_account)
        if self.contra_account.domain is None:
            self.contra_account.domain = [domain_fiscalyear]
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.contra_account.domain):
                self.contra_account.domain += [domain_fiscalyear]

        if 'fiscalyear' not in self.contra_account.depends:
            self.contra_account.depends = copy.copy(
                self.contra_account.depends)
            self.contra_account.depends.append('fiscalyear')

        self._reset_columns()

    def default_get(self, fields, with_rec_name=True):
        date_obj = Pool().get('ir.date')

        values = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)

        date = values.get('date', date_obj.today())
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)
        return res

    def on_change_account(self, vals):
        date_obj = Pool().get('ir.date')
        date = vals.get('date', date_obj.today())
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).on_change_account(vals)
        return res

    def on_change_party(self, value):
        date_obj = Pool().get('ir.date')
        date = value.get('date', date_obj.today())
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).on_change_party(value)
        return res

    def on_change_date(self, value):
        date_obj = Pool().get('ir.date')
        date = value.get('date', date_obj.today())
        with Transaction().set_context(effective_date=date):
            res = super(Line, self).on_change_date(value)
        return res

    def check_unposted_batch_lines_for_account(self, account_ids=None):
        account_obj = Pool().get('account.account')
        if not account_ids:
            return super(Line,
                self).check_unposted_batch_lines_for_account(account_ids)

        ids = set(account_ids)
        all_ids = copy.copy(ids)
        for account in account_obj.browse(list(ids)):
            for account in account.all_predecessors:
                all_ids.add(account.id)
            for account in account.all_successors:
                all_ids.add(account.id)

        all_ids = list(all_ids)

        res = super(Line, self).check_unposted_batch_lines_for_account(all_ids)

        if res:
           return True
        return False

    def create_move_dict(self, batch_line_vals):
        with Transaction().set_context(effective_date=batch_line_vals['date']):
            res = super(Line, self).create_move_dict(batch_line_vals)
        return res

    def _write_move_line(self, line):
        with Transaction().set_context(effective_date=line.date):
            res = super(Line, self)._write_move_line(line)
        return res

    def _get_lines_carry_forward(self, account):
        move_line_obj = Pool().get('account.move.line')

        res = []
        if account.party_is_mandatory:
            with Transaction().set_context(
                    company=account.company,
                    digits=account.company.currency.digits,
                    fiscalyear=account.fiscalyear):
                line_query, _ = move_line_obj.query_get()

            cursor = Transaction().cursor
            cursor.execute('SELECT l.party, SUM(l.debit), SUM(l.credit) ' \
                'FROM account_move_line l ' \
                    'JOIN account_move m ON (l.move = m.id) '
                    'JOIN account_account a ON (l.account = a.id) '
                'WHERE l.party IS NOT NULL '\
                    'AND a.id IN (%s) ' \
                    'AND l.move NOT IN (' \
                        # Sort out opening balance moves
                        'SELECT id FROM account_move m ' \
                        'WHERE m.journal IN (' \
                            'SELECT id FROM account_journal j ' \
                            "WHERE j.type = 'situation') " \
                            'AND m.period NOT IN (' \
                                # but not opening balance moves of the first
                                # fiscal year
                                'SELECT id from account_period ' \
                                'WHERE fiscalyear IN (' \
                                    'SELECT id from account_fiscalyear f ' \
                                    'ORDER BY f.start_date ASC LIMIT 1)))' \
                    'AND ' + line_query + ' ' \
                'GROUP BY l.party ' \
                'HAVING (SUM(l.debit) - SUM(l.credit) != 0)',
                (','.join(map(str, [
                    x.id for x in account.predecessors] + [account.id,])),))

            party_balances = cursor.fetchall()

            res = []
            for party_balance in party_balances:
                party, debit, credit = party_balance
                res.append({
                    'amount': debit - credit,
                    'party': party,
                    'contra_account': account.successor.id,
                })
        else:
            res.append({
                'amount': account.balance,
                'contra_account': account.successor.id,
            })

        return res

Line()


class CarryBalancesForwardInit(ModelView):
    'Carry Balances Forward Init'
    _name = 'account.batch.carry_balances_forward.init'
    _description = __doc__

    company = fields.Many2One('company.company', 'Company', required=True,
        on_change=['company', 'batch'], depends=['batch'])
    from_fiscalyear = fields.Many2One('account.fiscalyear', 'From Fiscalyear',
        required=True, on_change=['company', 'from_fiscalyear'],
        domain=[('company', '=', Eval('company'))], depends=['company',
            'from_fiscalyear'])
    to_fiscalyear = fields.Many2One('account.fiscalyear', 'To Fiscalyear',
        required=True, domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    batch = fields.Many2One('account.batch', 'Batch', required=True,
        domain=[
            ('journal.company', '=', Eval('company')),
            ('journal.account_journal.type', '=', 'situation'),
        ], depends=['company'])
    accounts = fields.Many2Many('account.account', None, None, 'Accounts',
        domain=[
            ('company', '=', Eval('company')),
            ('fiscalyear', '=', Eval('from_fiscalyear')),
            ('deferral', '=', True),
            ('kind', '!=', 'view'),
        ], required=True, depends=['company', 'from_fiscalyear'])

    def default_company(self):
        return Transaction().context.get('company') or False

    def on_change_from_fiscalyear(self, vals):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        res = {'to_fiscalyear': False}
        if vals.get('from_fiscalyear'):
            from_fiscalyear = fiscalyear_obj.browse(vals['from_fiscalyear'])
            to_fiscalyear = fiscalyear_obj.search([
                ('start_date', '=', (from_fiscalyear.end_date +
                    datetime.timedelta(days=1)))
                ], limit=1)
            if to_fiscalyear:
                res['to_fiscalyear'] = to_fiscalyear[0]
        return res

    def on_change_company(self, vals):
        batch_obj = Pool().get('account.batch')
        res = {'batch': False}
        if vals.get('company') and not vals.get('batch'):
            batch_ids = batch_obj.search([
                ('journal.company', '=', vals['company']),
                ('journal.account_journal.type', '=', 'situation'),
                ])
            if len(batch_ids) == 1:
                res['batch'] = batch_ids[0]
        return res

CarryBalancesForwardInit()


class CarryBalancesForward(Wizard):
    'Carry Balances Forward'
    _name = 'account.batch.carry_balances_forward'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.batch.carry_balances_forward.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('carry_forward', 'Ok', 'tryton-ok'),
                ],
            },
        },
        'carry_forward': {
            'result': {
                'type': 'action',
                'action': '_action_carry_forward',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(CarryBalancesForward, self).__init__()
        self._error_messages.update({
            'account_successor_missing': "Account '%s - %s' must have "
                'a successor.',
            'multiple_opening_balance_lines': 'There are more than one draft '
                "opening balance batch lines for account '%s - %s'.",
            'no_opening_balance_account_other': 'No opening balance account of '
                "kind 'other' found.",
            })

    def _action_carry_forward(self, data):
        pool = Pool()
        batch_obj = pool.get('account.batch')
        batch_line_obj = pool.get('account.batch.line')
        fiscalyear_obj = pool.get('account.fiscalyear')
        account_obj = pool.get('account.account')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        if not data['form']['accounts']:
            return {}

        batch = batch_obj.browse(data['form']['batch'])
        fiscalyear = fiscalyear_obj.browse(data['form']['to_fiscalyear'])
        opening_accounts = {}
        account_ids = account_obj.search([
            ('fiscalyear', '=', fiscalyear.id),
            ('opening_balance_account', '=', True),
            ])
        if account_ids:
            accounts = account_obj.browse(account_ids)
            for account in accounts:
                opening_accounts.setdefault(account.kind, account.id)
        if not opening_accounts.get('other'):
            self.raise_user_error('no_opening_balance_account_other')

        used_line_ids = []
        for account in account_obj.browse(data['form']['accounts'][0][1]):
            if not account.successor:
                self.raise_user_error('account_successor_missing',
                    error_args=(account.rec_name, account.fiscalyear.name))
            if account.balance == Decimal('0.0'):
                continue
            lines_vals = batch_line_obj._get_lines_carry_forward(account)
            for vals in lines_vals:
                vals['batch'] = batch.id
                vals['date'] = fiscalyear.start_date
                vals['account'] = (opening_accounts.get(account.kind) or
                    opening_accounts['other'])
                vals['journal'] = batch.journal.id
                vals['fiscalyear'] = account.successor.fiscalyear.id

                batch_line_ids = batch_line_obj.search([
                    ('state', '=', 'draft'),
                    ('party', '=', vals.get('party', False)),
                    ['OR',
                        ('account', '=', account.successor.id),
                        ('contra_account', '=', account.successor.id),
                    ],
                    ['OR',
                        ('account.opening_balance_account', '=', True),
                        ('contra_account.opening_balance_account', '=', True),
                    ]])
                if batch_line_ids:
                    if len(batch_line_ids) > 1:
                        self.raise_user_error('multiple_opening_balance_lines',
                            error_args=(account.successor.rec_name,
                                account.successor.fiscalyear.name))
                    batch_line_obj.write(batch_line_ids[0], vals)
                    used_line_ids.append(batch_line_ids[0])
                else:
                    new_id = batch_line_obj.create(vals)
                    used_line_ids.append(new_id)

        act_window_id = model_data_obj.get_id('account_batch',
            'act_batch_line_form')
        res = act_window_obj.read(act_window_id)
        domain = [('id', 'in', used_line_ids)]
        res['domain'] = str(domain)
        res['pyson_domain'] = PYSONEncoder().encode(domain)
        return res

CarryBalancesForward()

